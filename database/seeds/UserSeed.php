<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //配合工厂来执行数据批量填充
        factory(\App\User::class,10)->create();
        //将id为1的数据修改成自己所熟悉的数据
        //测试获取用户表中id为1的数据
        $user = User::find(1);
        $user->nickname = '后盾人';
        $user->email = '704311329@qq.com';
        $user->save();
    }
}
