<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id'); //定义表的主键
            $table->string('nickname'); //生成字段类型为字符串的字段,括号内的是字段名称
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();//自动创建出来2个时间相关字段,一个是数据添加时候的时间,一个是数据修改时候的时间,并且这个两个时间,不需要我们在数据处理的时候处理它,它会随着你的数据是增加还是编辑来对对应的字段进行处理
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
