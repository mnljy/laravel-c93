<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//你需要对哪个表进行工厂化生产数据,第一个参数就是这个表对应的模型类
$factory->define(App\User::class, function (Faker $faker) {
    return [
        'nickname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('admin888'),
        'remember_token' => str_random(10),
    ];
});
