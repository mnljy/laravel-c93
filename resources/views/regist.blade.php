{{--使用@extends()来继承父级模板,父级模板还是存放在resources/views里面--}}
@extends('public.father')
{{--使用   来替换父级模板中的占位符--}}
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{route('regist')}}" method="post">
        @csrf
        <div class="card">
            <div class="card-header">
                用户注册
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>昵称</label>
                    <input type="text" class="form-control" name="nickname" placeholder="请输入昵称...">
                </div>
                <div class="form-group">
                    <label>邮箱地址</label>
                    <input type="text" class="form-control" name="email" placeholder="请输入邮箱地址...">
                </div>
                <div class="form-group">
                    <label>密码</label>
                    <input type="password" class="form-control" name="password" placeholder="请输入密码">
                </div>
                <div class="form-group">
                    <label>确认密码</label>
                    <input type="password" class="form-control" name="password_confirmation" placeholder="请输入确认密码">
                </div>
            </div>
            <div class="card-footer text-muted">
                <button type="submit" class="btn btn-primary">立即注册</button>
            </div>
        </div>
    </form>

@endsection