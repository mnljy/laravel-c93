<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">laravel</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">后盾人</a>
                </li>
            </ul>
            {{--如果使用laravel框架提供的Auth类的用户认证方法来判断登录是否成功,如果成功,框架会自动在Auth类中将当前用户的数据存起来,所以这里我们可以直接从Auth类中获取当前已经认证过的用户数据来判断--}}
            @if(Auth::check())
                <a href="" class="btn btn-danger my-2 my-sm-0 mr-2 ">{{Auth::user()->nickname}}</a>
                <a href="{{route('logout')}}" class="btn btn-success my-2 my-sm-0">退出登录</a>
            @else
            <a href="{{route('regist')}}" class="btn btn-danger my-2 my-sm-0 mr-2 ">注册</a>
            <a href="{{route('login')}}" class="btn btn-success my-2 my-sm-0">登录</a>
            @endif
            </form>
        </div>
    </nav>
    {{--将父级模板中不是固定重复出现的代码,使用laravel框架提供的一个占位符来代替,然后在子模板中使用对应标签替换对应内容--}}
    @include('public._message')
    @yield('content')
</div>
</body>
</html>