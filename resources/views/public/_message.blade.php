{{--展示保存的闪存信息--}}
@foreach(['success','danger'] as $v)
    @if(session()->has($v))
        <div class="alert alert-{{$v}}" role="alert">
            <strong>{{session($v)}}</strong>
        </div>
    @endif
@endforeach
{{--展示保存的闪存信息--}}