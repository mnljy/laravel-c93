<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Route类定义一个get请求的路由规则,
 * 第一个参数是定义的路由规则
 * 第二个参数,可以是一个回调方法,也可以指向我们希望找到的控制器里面的方法
 */
/**
 * view()方法是用来渲染模板的方法,在方法内需要传递模板的文件路径及名称
 * 默认的模板路径的起点在当前项目的resrouces/views里面
 * .blade.php是laravel框架的模板命名规范,如果你定义的文件的模板,就必须以.blade.php结尾
 */
//Route::get('/', function () {
//    return view('a.b.abc');
//});
//通过路由指向一个控制器的方法
Route::get('/', 'EntryController@index');
//加载注册模板路由
Route::get('/regist','RegistController@regist')->name('regist');
//处理注册路由
Route::post('/regist','RegistController@store')->name('regist');
//加载登录模板路由
Route::get('/login','LoginController@loginForm')->name('login');
//处理登录路由
Route::post('/login','LoginController@login')->name('login');
//退出路由
Route::get('/logout','LoginController@logout')->name('logout');




