<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * 设置允许批量填充的字段名称,方法$fillable属性的数组中的字段名称,是允许被批量填充的
     * @var array
     */
//    protected $fillable = [
//        'nickname', 'email', 'password',
//    ];

    //设置不允许批量填充的字段名称,如果填写在数组内的是不允许填充的,其他的就都允许填充
    protected $guarded = ['password_confirmation'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
