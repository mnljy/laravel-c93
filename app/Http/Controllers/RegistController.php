<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistRequest;
use App\User;
use Illuminate\Http\Request;
use Validator;
class RegistController extends Controller
{
    /**
     *
     * 加载注册模板
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function regist(){
        //加载注册模板
        return view('regist');
    }

    /**
     * 处理注册功能方法
     */
    public function store(Request $request,RegistRequest $registRequest){
        //用$request对象来获取post请求的数据
//        $post = $request->all();
//        //进入到方法,应该来进行注册功能
//        //先对用户注册时候填写的数据进行一波判断
//        //使用框架提供的验证类,来创建和定义一个验证方法
//        //第一个参数,指定要验证的是谁
//        //第二个参数,就是制定验证规则
//        1.昵称,邮箱,密码,确认密码都不能为空,也就是必填

//        2.邮箱的格式必须正确

//        3.密码长度不能小于6位,不能大于20位,并且两次密码要相同
//        $validator = Validator::make($post,[
//            'nickname' => 'required',
//            'email' => 'required|email',
//            'password' => 'required|between:6,20|confirmed',
//            'password_confirmation' => 'required|between:6,20',
//        ]);
//        //如果检测有错误返回值为真,代表最少有一个验证规则不满足,这个时候,就应该提示用户错误信息,并且返回
//        if ($validator->fails()){
//            //如果跳转的时候直接写路由规则,就在redirect方法内传递路由规则参数,
//            //如果想使用路由别名来调用路由,在redirect()方法内不传参数,后面继续链式调用route方法来指定路由名字跳转
////            return redirect('/regist');
//            return redirect('/regist')->withErrors($validator);
//        }
        //如果使用表单验证类来完成验证,能走进当前方法,代表验证通过,如果验证有一个不通过,都会被自动打回去
        //直接对post数据进行获取并存入数据库中
        $post = $request->all();
        //laravel框架默认不允许一次性给一条数据添加多个字段值,
        //将post数据中的password加密
        $post['password'] = bcrypt($post['password']);
        User::create($post);
        //设置闪存信息
        session()->flash('success','注册成功!!请登录');
        //返回前台首页,
        return redirect('/');
//
    }


}
