<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /**
     * 加载登录模板方法
     */
    public function loginForm(){
        return view('login');
    }

    public function login(Request $request){
            //判断用户的账号密码是否正确
            //使用Auth类的attempt方法来进行验证
            $status = Auth::attempt([
                'email' => $request->input('email'),
            'password' => $request->input('password'),
            ]);
        //利用验证返回的真和假来判断用户是否登录成功,如果返回真,代表账号密码都正确,登录成功,反之,登录失败
        if ($status){
            //设置成功闪存信息,并返回首页
            session()->flash('success','登录成功!!!欢迎回家!!!');
            return redirect('/');
        }else{
            //设置错误闪存信息,并返回登录页面
            session()->flash('danger','账号或密码不正确!!兄弟,你自己猜吧!!!');
            return redirect()->route('login');
        }
    }

    public function logout(){
        Auth::logout();
        //设置闪存信息,并跳转
        session()->flash('success','退出成功!!欢迎下次再来!!');
        return redirect()->route('login');
    }
}
