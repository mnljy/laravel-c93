<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //如果你需要使用表单验证类来完成表单验证,需要将该方法的返回值改成true
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname' => 'required',
            'email' => 'required|email',
            'password' => 'required|between:6,20|confirmed',
            'password_confirmation' => 'required|between:6,20',
        ];
    }

    //如果想将错误提示变成中文,可以在表单验证类中重写messages方法,注意:这里重写的方法一定只能叫messages
//    public function messages()
////    {
////        return [
////            //格式是 a => b ,a写的是被验证的字段名.验证规则,b写的是中文的错误提示
////            'nickname.required' => '兄die,昵称不能为空',
////            'email.required' => '兄die,邮箱不能为空',
////            'email.email' => '兄die,邮箱格式不正确',
////            'password.required' => '兄die,密码不能为空',
////            'password.between' => '兄die,密码长度要再6-20位之间',
////            'password.confirmed' => '兄die,两次密码输入不一致!!!',
////            'password_confirmation.required' => '兄die,确认密码不能为空',
////            'password_confirmation.between' => '兄die,确认密码长度要在6-20位之间',
////        ];
////    }

}
